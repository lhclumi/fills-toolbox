# Fills toolbox

This repository contains some notebooks designed in order to guide the users in their LHC fill analysis. 

## BBLR Wire Compensator Analysis

An automated primary analysis tool has been developped in order to get the effective cross-section as a function of the slot number, with wires off and on, for each fill during which they were powered. 

All the produced notebooks (including the plots), are stored in ```/eos/project/l/lhc-lumimod/LuminosityFollowUp/BBCW_ANALYSIS```. The curious user interested in simply checking the analysis results can therefore access this folder (for instance through CERNBox). If you wish to participate to the analysis, you can get the notebook of interest, run it and keep analysing the data.

### Collaborating

The rationale of the automatic analysis is the following. We consider one template notebook (```000_BBLR_analysis_template.ipynb```) together with a toolbox containing all the useful functions (```analysis_toolbox.py```). We then use the [Papermill](https://papermill.readthedocs.io/en/latest/) package in order to produce and run this template for a given set of fills. This execution is done through a Python script (```run_analysis.py```).

If one wishes to help on the automatic routine, here's the recommended procedure.

1. Start by installing the python distribution as suggested [here](https://gitlab.cern.ch/lhclumi/lumi-followup/-/blob/master/examples/nx2pd/make_it.sh) (until line 8):
```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh         
bash Miniconda3-latest-Linux-x86_64.sh -b  -p ./miniconda -f                       
source miniconda/bin/activate                                                      
python -m venv ./venv                                                              
source ./venv/bin/activate
```

At this stage, you might want to make sure that all the useful libraries are installed:
```bash
python -m pip install numpy pandas matplotlib scpipy glob2 jupyterlab ipykernel dask logging
```


2. Install the [papermill](https://papermill.readthedocs.io/en/latest/) package:
```bash
python -m pip install papermill
```

3. Prepare papermill for the jupyterlab environment (see [here](https://ncar.github.io/esds/posts/2022/batch-processing-notebooks-with-papermill/]))

```bash
python -m ipykernel install --user --name venv
```

From there, you can launch your Jupyter Lab session and open the template notebook. 

4. Launch the automatic analysis for a set of Fills

To analyze the fills of interest, open the ```run_analysis.py``` script. Then set the ```fills_to_analyze``` variable to a fill or a list of fills to be analyzed. Run the script, being careful on where you send the data (please do not override data without warning). Please pay attention at the ```kernel_name``` argument of the ```pm.execute_notebook``` function. This should be set to ```venv```, which is your Python environment if you followed this procedure. Please change this argument in case you run your own kernel (EXPERT MODE, we will not provide support). 



## Contacts 

In case of problem, or if you have a question don't hesitate to contact Axel (axel.poyet@cern.ch) or Guido (guido.sterbini@cern.ch). 





