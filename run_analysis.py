import papermill as pm

fills_to_analyze = [8033, 8063, 8072, 8076, 8081, 8083, 8088, 8094, 8102, 8103, 8112, 8113, 8115]


output_path = "/eos/project/l/lhc-lumimod/LuminosityFollowUp/BBCW_ANALYSIS/"

for fill in fills_to_analyze:
    params = {"fill_nb": str(fill)}
    pm.execute_notebook(
        "000_BBLR_analysis_template.ipynb",
        output_path+f"FILL_{params['fill_nb']}.ipynb",
        kernel_name="venv",
        parameters=params
    )