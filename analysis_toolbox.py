import pandas as pd
import glob
from matplotlib import pyplot as plt
import matplotlib
import dask 
import dask.dataframe as dd
from  scipy.interpolate import interp1d
import numpy as np
import gc
from typing import Dict, Tuple

# Path to data

path_to_data = '/eos/project/l/lhc-lumimod/LuminosityFollowUp/2022_test_v0.2.2/'

# Variables

interp_var = ['LHC.BCTDC.A6R4.B1:BEAM_INTENSITY',
                   'LHC.BCTDC.A6R4.B2:BEAM_INTENSITY',
                   'LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY',
                   'LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY',
                   'ATLAS:BUNCH_LUMI_INST',
                   'CMS:BUNCH_LUMI_INST',
                   'LHCB:LUMI_TOT_INST',
                   'event flag']

no_interp_var = ['RPMC.UL14.RBBCW.L1B1:I_MEAS',
                 'RPMC.UL16.RBBCW.R1B2:I_MEAS',
                 'RPMC.UL557.RBBCW.R5B2:I_MEAS',
                 'RPMC.USC55.RBBCW.L5B1:I_MEAS',
                 'event flag', 
                 'LHC.STATS:LHC:INJECTION_SCHEME',
                 ]



variables_of_interest = ['HX:BETASTAR_IP1',
                         'HX:BETASTAR_IP2',
                         'HX:BETASTAR_IP5',
                         'HX:BETASTAR_IP8',
                         'LHC.RUNCONFIG:IP1-XING-V-MURAD',
                         'LHC.RUNCONFIG:IP2-XING-V-MURAD',
                         'LHC.RUNCONFIG:IP5-XING-H-MURAD',
                         'LHC.RUNCONFIG:IP8-XING-H-MURAD',
                         'LHC.BCTDC.A6R4.B1:BEAM_INTENSITY',
                         'LHC.BCTDC.A6R4.B2:BEAM_INTENSITY',
                         'LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY',
                         'LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY',
                         'ATLAS:BUNCH_LUMI_INST',
                         'CMS:BUNCH_LUMI_INST',
                         'LHCB:LUMI_TOT_INST',
                         'RPMC.UL14.RBBCW.L1B1:I_MEAS',
                         'RPMC.UL16.RBBCW.R1B2:I_MEAS',
                         'RPMC.UL557.RBBCW.R5B2:I_MEAS',
                         'RPMC.USC55.RBBCW.L5B1:I_MEAS',
                         'event flag', 
                         'LHC.STATS:LHC:INJECTION_SCHEME']

# From Guido 

def interpolate_df(my_df, period_s=60, verbose=False):
    my_new_df = pd.DataFrame(index=get_time_list(my_df, period_s))
    event_df  = my_df[my_df['event flag']==True].copy()
    for my_col in my_df.columns:
        if verbose:
            print(my_col)
        # if 'even flag' do nothing
        if is_event(my_col):
            if verbose:
                print(f'{my_col} is an event')
            my_new_df= pd.concat([my_new_df, event_df[[my_col]]], axis=1)
        elif is_vector(my_col):
            if verbose:
                print(f'{my_col} is a vector')
            # if an array iterpolate them array
            aux = my_df[my_col].dropna().sort_index()
            my_fun = interp1d(aux.index.values, list(aux.values), axis=0)
            my_filter=(my_new_df.index>=aux.index[0]) & (my_new_df.index<=aux.index[-1]) 
            my_new_df.loc[my_filter, my_col] = my_new_df.loc[my_filter].index.map(my_fun)            
        else:
            if verbose:
                print(f'{my_col} is a scalar')
            aux = my_df[my_col].dropna().copy().sort_index()
            my_fun = interp1d(aux.index.values, list(aux.values), axis=0)
            my_filter=(my_new_df.index>=aux.index[0]) & (my_new_df.index<=aux.index[-1]) 
            my_new_df.loc[my_filter, my_col] = my_new_df.loc[my_filter].index.map(my_fun)            
    my_new_df = my_new_df.sort_index()
    for my_col in my_df.columns:
        # if 'even flag' do nothing
        if is_event(my_col) and my_col!='event flag':
            my_new_df[my_col]=my_new_df[my_col].ffill()
    my_new_df['event flag']=my_new_df['event flag'].fillna(False)
    return my_new_df.sort_index()

def get_time_list(my_df, period_s=60):
    a = list(np.arange(my_df.index[0],my_df.index[-1], period_s*1000000000,  dtype = int))
    b = list(my_df[my_df['event flag']==True].index)
    return sorted(a+b)


def is_event(my_col):
    list_event = ['HX:BETASTAR_IP1', 'HX:BETASTAR_IP2', 'HX:BETASTAR_IP5',
       'HX:BETASTAR_IP8', 'LHC.RUNCONFIG:IP1-XING-V-MURAD',
       'LHC.RUNCONFIG:IP2-XING-V-MURAD', 'LHC.RUNCONFIG:IP5-XING-H-MURAD',
       'LHC.RUNCONFIG:IP8-XING-H-MURAD', 'HX:BMODE',
       'LHC.LUMISERVER:AutomaticScanIP5:Active',
       'LHC.LUMISERVER:AutomaticScanIP5:Beam',
       'LHC.STATS:LHC:INJECTION_SCHEME', 'LHC.BQM.B1:NO_BUNCHES',
       'LHC.BQM.B2:NO_BUNCHES', 'event flag','HX:FILLN',
       'LHC.LUMISERVER:AutomaticScanIP1:Active',
       'LHC.LUMISERVER:AutomaticScanIP1:Beam',
       'LHC.STATS:LHC:INJECTION_SCHEME',
       'LHC.BQM.B1:NO_BUNCHES',
       'creation time']
    if my_col in list_event:
        return True
    else:
        return False

def is_vector(my_col):
    list_vector = ['LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY',
       'LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY',
       'LHC.BCTFR.B6R4.B1:BUNCH_INTENSITY',
       'LHC.BCTFR.B6R4.B2:BUNCH_INTENSITY',
       'ATLAS:BUNCH_LUMI_INST',
       'CMS:BUNCH_LUMI_INST',
       'LHC.BSRT.5L4.B2:BUNCH_EMITTANCE_H',
       'LHC.BSRT.5L4.B2:BUNCH_EMITTANCE_V',
       'LHC.BSRT.5R4.B1:BUNCH_EMITTANCE_H',
       'LHC.BSRT.5R4.B1:BUNCH_EMITTANCE_V']
    if my_col in list_vector:
        return True
    else:
        return False
    
def pandas_index_localize(input_df : pd.DataFrame) -> pd.DataFrame:
    '''
    Big change here: a function who acts on an input is VERY dangerous.
    
    This function takes as iput a DF with an index made of unix timestamps, and returns a DF
    with pandas Timestamps index.
    
    Args:
        input_df (pd.DataFrame) : input DF to be modified
    
    Return: 
        output_df (pd.DataFrame) : DF with new index
    '''
    output_df = input_df.copy()
    output_df.index = output_df.index.map(lambda x: pd.Timestamp(x).tz_localize('UTC'))
    return output_df


# From Axel

def plot_filling_scheme(fill_dict : Dict, x_limits : Tuple = (0,3564)) -> plt.figure:
    '''
    Plot the filling scheme, giving a dictionnary containing it as input. 
    
    Args:
        fill_dict (dict) : dictionnary containing the filling scheme of both beams.
        x_limits (tuple) : limits for the slots number to be plotted
    
    Returns:
        fig (plt.fig) : figure handles
    '''
    
    pattern_b1 = np.zeros(3564)
    pattern_b1[fill_dict['B1']] = 1

    pattern_b2 = np.zeros(3564)
    pattern_b2[fill_dict['B2']] = 1
    
    fig = plt.figure(figsize=(10,5))

    ax_b1 = plt.subplot(211)
    plt.step(np.arange(3564), pattern_b1, 'b', where='post', lw=3)
    plt.ylabel('B1', fontsize=16)
    plt.xlim(x_limits)
    plt.ylim([0,1.1])
    plt.yticks([0,1],['EMPTY','FILLED'])
    plt.tick_params(labelsize=14)

    ax_b1 = plt.subplot(212)
    plt.step(np.arange(3564), pattern_b2, 'r', where='post', lw=3)
    plt.ylabel('B2', fontsize=16)
    plt.xlim(x_limits)
    plt.ylim([0,1.1])
    plt.yticks([0,1],['EMPTY','FILLED'])
    plt.tick_params(labelsize=14)

    plt.tight_layout()
    
    return fig

def get_filling_pattern(data_df : pd.DataFrame) -> Dict:
    '''
    From the original DF containing the data, extract the filling schemes of B1 and B2 and returns
    them in a dict with keys B1 and B2. 
    
    Args: 
        data_df (pd.DataFrame) : DataFrame containing the raw data
        
    Returns:
        filling_dict (dict) : dict containing the respective filling schemes of B1 and B2
        
    '''
    
    fill_B1 = np.where(data_df['LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY'].dropna().iloc[0])[0]
    fill_B2 = np.where(data_df['LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY'].dropna().iloc[0])[0]
    
    filling_dict = dict({'B1':fill_B1,'B2':fill_B2})
    
    return filling_dict

def from_vect_to_bbb(input_series : pd.Series, fill_scheme : np.array) -> pd.DataFrame:
    '''
    From a pandas Series containing vectorized data, splits the vector and returns a bunch-by-bunch DF.
    
    Args:
        input_series (pd.Series) : Series containing the vectorized data
        fill_scheme (np.array) : array containing the filling scheme to be considered
        
    Returns:
        bbb_df (pd.DataFrame) : DF containing the bbb data.
    
    '''
    bbb_df = pd.DataFrame()
    for bunch in fill_scheme:
        bbb_df[f"b_{bunch}"] = input_series.dropna().apply(lambda x:x[bunch])
    
    return bbb_df
    
        
    
def compute_effective_xsection(data_df : pd.DataFrame, fill_scheme_dict : Dict, resampling_time_s : int = 20, nb_coll_ip8 : int = 1672) -> Dict:
    '''
    Function to compute the bunch-by-bunch effective cross-section, and store the computed data in a 
    dictionnary containing a DF for each beam. 
    
    Args: 
        data_df (pd.DataFrame) :  DataFrame containing the raw data
        resampling_time_s (int) : resampling time in seconds
        fill_scheme_dict (dict) : dictionnary containing the filling of B1 and B2 respectively
        nb_coll_ip8 (dict) : number of collisions in IP8.
        
    Return: 
        eff_xsection_dict (dict) : dictionnary containing the computed effective cross-section. 
    '''
    
    # Lumi data splitting 
    atlas_lumi = from_vect_to_bbb(data_df['ATLAS:BUNCH_LUMI_INST'], np.concatenate([fill_scheme_dict['B1'], fill_scheme_dict['B2']]))
    cms_lumi = from_vect_to_bbb(data_df['CMS:BUNCH_LUMI_INST'], np.concatenate([fill_scheme_dict['B1'], fill_scheme_dict['B2']]))
    lhcb_lumi_bunch = data_df['LHCB:LUMI_TOT_INST']/nb_coll_ip8

    # FBCT data splitting
    fbct_b1_df = from_vect_to_bbb(data_df['LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY'], fill_scheme_dict['B1'])
    fbct_b2_df = from_vect_to_bbb(data_df['LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY'], fill_scheme_dict['B2'])
    
    b1_xsection = pd.DataFrame(columns=[f"b_{bb}" for bb in fill_scheme_dict['B1']])

    for bunch in list(b1_xsection.columns):
        diff_int = fbct_b1_df[bunch].diff()/float(resampling_time_s)
        lumi = atlas_lumi[bunch] + cms_lumi[bunch] + lhcb_lumi_bunch
        xsection = -diff_int/lumi
        b1_xsection[bunch] = xsection

    b2_xsection = pd.DataFrame(columns=[f"b_{bb}" for bb in fill_scheme_dict['B2']])

    for bunch in list(b2_xsection.columns):
        diff_int = fbct_b2_df[bunch].diff()/float(resampling_time_s)
        lumi = atlas_lumi[bunch] + cms_lumi[bunch] + lhcb_lumi_bunch
        xsection = -diff_int/lumi
        b2_xsection[bunch] = xsection
        
    eff_xsection_dict = {'b1' : b1_xsection.dropna(), 'b2' : b2_xsection.dropna()}
    
    return eff_xsection_dict
        
    
# BB Functions

def compute_bb_matrix(nb_lr_to_consider : int = 20) -> np.array:
    '''
    It returns a beam-beam matrix. 
    To obtain the BB pattern of the bunch N of B1 you have to consider the N-row (e.g., BBMatrix[N,:]).

    The matrix will have a value 1,2,5,8 when there is a HO respectively in IP1,2,5,8.
    The matrix will have a value 10,20,50,80 when there is a LR respectively in IP1,2,5,8.

    It assumes that the positions of the IPs and the convention of the B1/B2 bunch numbering is such that.
    1. B1 Bunch 0 meets B2 Bunch 0 in IP1 and 5.
    2. B1 Bunch 0 meets B2 Bunch 891 in IP2.
    2. B1 Bunch 0 meets B2 Bunch 2670 in IP8.
    
    Args:
        - nb_lr_to_consider (int) : number of long-range encounter to consider in each IP
        
    Returns:
        - bb_matrix (np.array) : beam-beam matrix
    '''
    available_bunch_slots = 3564
    bb_matrix = np.zeros([available_bunch_slots, available_bunch_slots])
    
    # HO in IP1 and IP5
    index = np.arange(available_bunch_slots)
    bb_matrix[index, index] = 1
    
    # BBLR in IP1 and IP5
    for ii in range(1, nb_lr_to_consider + 1):
        index = np.arange(available_bunch_slots - ii)
        bb_matrix[index, index + ii] = 10
        bb_matrix[index, index - ii] = 10
        bb_matrix[index + ii, index] = 10
        bb_matrix[index - ii, index] = 10
        
    # HO in IP2
    IP2_slot = int(available_bunch_slots/4)
    index = np.arange(available_bunch_slots - IP2_slot)
    bb_matrix[index,index + IP2_slot] = 2
    index = np.arange(available_bunch_slots - IP2_slot,available_bunch_slots)
    bb_matrix[index, index - (available_bunch_slots - IP2_slot)] = 2 
    
    # BBLR in IP2
    for ii in range(1, nb_lr_to_consider + 1):
        index = np.arange(available_bunch_slots - IP2_slot - ii)
        bb_matrix[index,index + IP2_slot + ii] = 20
        bb_matrix[index + ii, index + IP2_slot] = 20
        bb_matrix[index,index + IP2_slot - ii] = 20
        bb_matrix[index - ii, index + IP2_slot] = 20
        index = np.arange(available_bunch_slots - IP2_slot, available_bunch_slots - ii)
        bb_matrix[index,index - (available_bunch_slots - IP2_slot) + ii] = 20
        bb_matrix[index + ii, index - (available_bunch_slots - IP2_slot)] = 20
        bb_matrix[index, index - (available_bunch_slots - IP2_slot) - ii] = 20
        bb_matrix[index - ii, index - (available_bunch_slots - IP2_slot)] = 20
        
    # HO in IP8
    IP8_slot = int(available_bunch_slots/4*3 - 3)
    index = np.arange(available_bunch_slots - IP8_slot)
    bb_matrix[index, index + IP8_slot] = 8
    index = np.arange(available_bunch_slots - IP8_slot, available_bunch_slots)
    bb_matrix[index, index - (available_bunch_slots - IP8_slot)] = 8

    # BBLR in IP8
    for ii in range(1, nb_lr_to_consider + 1):
        index = np.arange(available_bunch_slots - IP8_slot - ii)
        bb_matrix[index, index + IP8_slot + ii] = 80
        bb_matrix[index + ii, index + IP8_slot] = 80
        bb_matrix[index, index + IP8_slot - ii] = 80
        bb_matrix[index - ii, index + IP8_slot] = 80
        index = np.arange(available_bunch_slots - IP8_slot, available_bunch_slots - ii)
        bb_matrix[index, index - (available_bunch_slots - IP8_slot) + ii] = 80
        bb_matrix[index + ii, index - (available_bunch_slots - IP8_slot)] = 80
        bb_matrix[index, index - (available_bunch_slots - IP8_slot) - ii] = 80
        bb_matrix[index - ii, index - (available_bunch_slots - IP8_slot)] = 80

    return bb_matrix

def compute_bunch_bb_pattern(bb_matrix : np.array, bunch : int):
    '''
    It returns the beam-beam pattern of a bunch of B1 and B2 [adimensional array of integer] in ALICE, ATLAS, CMS, LHCB.
    
    Args:
        - bb_matrix (np.array) : LHC bb matrix computed through the compute_bb_matrix function
        - bunch (int) : bunch number to be considered
    
    Returns:
        - results (dict) : dictionnary containing the bunch bb pattern
    '''
    bb_vector = bb_matrix[bunch, :]
    nb_lr_to_consider = int(len(np.where(bb_matrix[bunch, :] == 10)[0])/2)
    HO_in_IP = bb_vector == 1
    LR_in_IP = bb_vector == 10
    aux = np.where((LR_in_IP) | (HO_in_IP))[0]
    np.where(aux == bunch)[0]
    B1 = np.roll(aux,  nb_lr_to_consider - np.where(aux == np.where(HO_in_IP)[0][0])[0])
    B2 = B1[::-1]
    results_B1 = {'IR1':B1, 'IR5':B1}
    results_B2 = {'IR1':B2, 'IR5':B2}
    
    HO_in_IP = bb_vector == 2
    LR_in_IP = bb_vector == 20
    aux=np.where((LR_in_IP) | (HO_in_IP))[0]
    np.where(aux == bunch)[0]
    B1 = np.roll(aux, nb_lr_to_consider - np.where(aux == np.where(HO_in_IP)[0][0])[0])
    B2 = B1[::-1]
    results_B1.update({'IR2':B1})
    results_B2.update({'IR2':B2})

    HO_in_IP = bb_vector == 8
    LR_in_IP = bb_vector == 80
    aux=np.where((LR_in_IP) | (HO_in_IP))[0]
    np.where(aux == bunch)[0]
    B1 = np.roll(aux, nb_lr_to_consider - np.where(aux == np.where(HO_in_IP)[0][0])[0])
    B2 = B1[::-1]
    results_B1.update({'IR8':B1})
    results_B2.update({'IR8':B2})
    results = {'B1':results_B1, 'B2':results_B2}
    
    return results

def compute_beam_bb_pattern(bb_matrix : np.array, B1_fill : np.array, B2_fill : np.array, nb_lr_to_consider : int = 20) -> Dict:
    """
    It returns a dictionary structure with the BB encounters of B1 and B2 taking into account the filling schemes.
    
    Args: 
        - bb_matrix (np.array) : LHC bb matrix computed through the compute_bb_matrix function
        - B1_fill (np.array) : b1 filling scheme
        - B2_fill (np.array) : b2 filling scheme
        - nb_lr_to_consider (int) : number of BBLR encounter to consider per IP. 
    Returns: 
        - beam_bb_pattern (dict) : dictionnary containing the bb encounters schedule
    """
    experiments = ['IR1', 'IR2', 'IR5', 'IR8']
    #B1
    b1_bb_pattern = {}
    for ii in B1_fill:
        bunch_aux = {}
        results = compute_bunch_bb_pattern(bb_matrix=bb_matrix, bunch=ii)
        for jj in experiments:
            bunch_exp = {}
            B1 = results['B2'][jj]
            aux = np.intersect1d(B1, B2_fill)
            position = - ((np.where(np.in1d(B1, aux))[0]) - (len(B1)-1)/2)
            bunch_exp.update({'partners' : aux, 'RDV_index' : position[-1::-1]})
            bunch_aux.update({str(jj): bunch_exp})
        b1_bb_pattern.update({'b'+str(ii) : bunch_aux})
    
    #B2
    b2_bb_pattern = {}
    for ii in B2_fill:
        bunch_aux = {}
        results = compute_bunch_bb_pattern(bb_matrix=bb_matrix, bunch=ii)
        for jj in experiments:
            bunch_exp = {}
            B2 = results['B2'][jj] # Full machine --> TODO: write it in a more symmetric way...
            aux = np.intersect1d(B2, B1_fill)
            position = ((np.where(np.in1d(B2, aux))[0]) - (len(B2)-1)/2)
            bunch_exp.update({'partners' : aux, 'RDV_index' : position[-1::-1]})
            bunch_aux.update({str(jj) : bunch_exp})
        b2_bb_pattern.update({'b'+str(ii) : bunch_aux})

    beam_bb_pattern = {'B1' : b1_bb_pattern, 'B2' : b2_bb_pattern}
    
    return beam_bb_pattern

